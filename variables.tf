variable "s3_bucket" {
}

variable "region" {
}

variable "stage" {
}

variable "name" {
    default = "awxsight-pypi"
}

variable "project" {
  description = "Project Name"
  default     = "AWXSIGHTPYPIService"
}

variable "product_tag" {
  description = "Name of the product"
  default = "AW Platform"
}

variable "team" {
  description = "Development Team"
  default = "PlatformServices"
}

variable "service" {
  description = "service name"
  default = "AWXSIGHTPYPI"
}

variable "artifact_version" {}

variable "owner" {
  description = "Service Owner"
  default     = "XSight-CI"
}

variable "remote_state_bucket_prefix" {
  description = "remote state bucket name prefix"
}

variable "account_id" {}