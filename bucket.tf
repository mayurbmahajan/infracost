resource "aws_s3_bucket" "createbucket" {
  bucket = "${local.bucket_name}"
  region = "${var.region}"
  acl    = "private"

  versioning {
    enabled = false
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  tags = "${local.tags}"
}


resource "aws_s3_bucket_policy" "policy" {
  bucket = "${aws_s3_bucket.createbucket.id}"
  policy = "${data.aws_iam_policy_document.bucket_policy_doc.json}"
}

resource "aws_s3_bucket_public_access_block" "bucket" {
  bucket = "${aws_s3_bucket.createbucket.id}"

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

}

data "aws_iam_policy_document" "bucket_policy_doc" {
  statement {
    principals {
      identifiers = ["${aws_iam_role.role.arn}"]
      type        = "AWS"
    }

    effect    = "Allow"
    actions   = ["s3:*"]
    resources = ["${local.bucket_arn}","${local.bucket_arn}/*"]
  }

  statement {
    effect = "Deny"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions   = ["s3:DeleteBucket"]
    resources = ["${local.bucket_arn}"]
    sid       = "DenyDeleteBucket"
  }
}
