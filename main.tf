terraform {
  backend "s3" {}
}


provider "aws" {
  region = "${var.region}"
}


data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "${local.remote_state_bucket}"
    key    = "network/terraform.tfstate"
    region = "${var.region}"
  }
}


locals {
    api_name                = "${var.name}gateway"
    bucket_name             = "${var.s3_bucket}-${var.region}-${var.stage}"
    bucket_arn              = "arn:aws:s3:::${local.bucket_name}"
    tags                    = "${merge(map("Name", "${var.project}", "Owner", "${var.owner}", "Project", "${var.project}", "Terraform", "true"))}"
    artifacts_bucket        = "xsight-artifacts-${var.region}-deployment"
    remote_state_bucket     = "${var.remote_state_bucket_prefix}-${var.region}-${var.stage}"
    vpc_id                  = "${data.terraform_remote_state.network.vpc-id}"
    subnets                 = "${slice(data.terraform_remote_state.network.private-subnets, 0, min(length(data.terraform_remote_state.network.azs), length(data.terraform_remote_state.network.private-subnets)))}"
}
