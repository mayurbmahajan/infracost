data "aws_s3_bucket_object" "aw_xsight_pypi_lambda_object"{
  bucket = "${local.artifacts_bucket}"
  key    = "lambda/awxsight-pypi-handler/awxsight-pypi-handler-${var.artifact_version}.zip"
}


resource "aws_lambda_function" "pypi_lambda" {
  function_name     = "AW_XSight_PYPI_Handler"
  s3_bucket         = "${data.aws_s3_bucket_object.aw_xsight_pypi_lambda_object.bucket}"
  s3_key            = "${data.aws_s3_bucket_object.aw_xsight_pypi_lambda_object.key}"
  handler           = "awxsight-pypi-handler/pypi_handler.lambda_handler"
  role              = "${aws_iam_role.role.arn}"
  runtime           = "python3.6"
  timeout           = 900
  memory_size       = 512
  
  environment {
    variables {
      BUCKET_NAME    = "${local.bucket_name}"
      REGION         = "${var.region}"
    }
  }
  vpc_config {
    security_group_ids = ["${aws_security_group.pypi_security_group.id}"]
    subnet_ids         = ["${local.subnets}"]
  }
  depends_on = ["aws_iam_role.role"]
}


resource "aws_lambda_permission" "apigw_lambda_pypi_lambda_permissions" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.pypi_lambda.function_name}"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.pypi_service_api.execution_arn}/*/*"
}
